# Test doubles

While unit testing , we focus on testing every code path within the unit of code. The direct input to the system under test is a way to test certain code path.

Along with testing the state of the SUT post test, it is worth verifying the impact due to the system.

However, there are several indirect inputs/impacts (output) with SUT that changes the behaviour/state of SUT which needs a through testing.

In cases where the indirect factors cannot be replicated consistently, it is wise to use test doubles.

In this repository, the system under test is in ./src/user.js

This SUT is tested for state and behaviour using four types of tst doubles, namely mock, stub, fake and spy.

Tests are available in ./test/

## Install
To install this project from scratch issue these commands
```
> # Clone the repository
> git clone https://bitbucket.org/sudharsan_sundarababu/test-doubles/
> cd test-doubles
>
> # Install the project
> # Please use node version 12.16.1 and above
> npm install
```

## Run tests
To run all the tests, issue this command
```
> npm run test
```
