const axios = require('axios');

module.exports = {
  getUser: async (user) => axios
    .get(`https://api.github.com/users/${user}`)
    .then((r) => r.data)
    .catch((e) => e),
};
