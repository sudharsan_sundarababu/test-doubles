const axios = require('axios');
const sinon = require('sinon');
const { describe, it, afterEach } = require('mocha');
const { expect } = require('chai');

const { getUser } = require('../src/user');

describe('Test getUser() with stub\ngetUser', async () => {
  let stub;

  afterEach(() => {
    sinon.restore();
    stub.restore();
  });

  it('should return user information for a valid user', async () => {
    // Test Setup
    stub = sinon.stub(axios, 'get');
    stub.returns(Promise.resolve({
      data: {
        login: 'dev',
        id: 12158001,
      }
    }));

    // Test Execution
    const userInfo = await getUser('dev');

    // Test Verifications
    expect(userInfo).to.have.property('login');
    expect(userInfo).to.have.property('id');
  });

  it('should return error for a non-existing user', async () => {
    // Test Setup
    stub = sinon.stub(axios, 'get');
    /* eslint-disable-next-line prefer-promise-reject-errors */
    stub.returns(Promise.reject({ message: 'User not found' }));

    // Test Execution
    const userInfo = await getUser('dev');

    // Test Verifications
    expect(userInfo).to.not.have.property('login');
    expect(userInfo).to.not.have.property('id');
    expect(userInfo).to.deep.equal({ message: 'User not found' });
  });
});
