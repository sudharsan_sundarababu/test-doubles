const axios = require('axios');
const sinon = require('sinon');
const { describe, it, afterEach } = require('mocha');
const { expect } = require('chai');

const { getUser } = require('../src/user');


describe('Test getUser() with mock\ngetUser', async () => {
  let mock;

  afterEach(() => {
    sinon.restore();
  });

  it('should return user information for a valid user', async () => {
    // Test Setup
    mock = sinon.mock(axios);
    mock.expects('get')
      .once()
      .withArgs('https://api.github.com/users/dev')
      .resolves({
        data: {
          login: 'dev',
          id: 12158001,
        }
      });

    // Test Execution
    const userInfo = await getUser('dev');

    // Test Verifications
    expect(userInfo).to.have.property('login');
    expect(userInfo).to.have.property('id');

    mock.verify();
  });

  it('should return error for a non-existing user', async () => {
    // Test setup
    mock = sinon.mock(axios);
    mock.expects('get') // Set behaviour expectation with the function getUser()
      .once()
      .withArgs('https://api.github.com/users/notavaliduser')
      .rejects({
        error: { message: 'User not found' }
      });

    // Test Execution
    const userInfo = await getUser('notavaliduser');
    expect(userInfo).to.not.have.property('login');
    expect(userInfo).to.not.have.property('id');
    expect(userInfo.error).to.deep.equal({ message: 'User not found' });

    // Test Verification
    mock.verify();
  });
});
