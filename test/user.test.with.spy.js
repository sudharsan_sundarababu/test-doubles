const axios = require('axios');
const { describe, it, afterEach } = require('mocha');
const { expect } = require('chai');
const sinon = require('sinon');

const { getUser } = require('../src/user');

describe('Test getUser() with spy\ngetUser', async () => {
  let spy;

  afterEach(() => {
    spy.restore();
  });

  it('should make exactly a single request to github api per invocation', async () => {
    // Test setup
    spy = sinon.spy(axios, 'get');

    // Test Execution
    await getUser('dev');

    // Test Verifications
    expect(spy.callCount, 1);
  });
});
