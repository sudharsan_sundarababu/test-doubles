const sinon = require('sinon');
const {
  describe, it, afterEach
} = require('mocha');
const { expect } = require('chai');

const { getUser } = require('../src/user');

describe('Test getUser() with fakeServer\ngetUser', async () => {
  let server;

  afterEach(() => {
    server.restore();
    sinon.restore();
  });

  it('should return user information for a valid user', async () => {
    // Test Setup
    server = sinon.fakeServer.create();
    setTimeout(() => {
      server.respond([
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          login: 'dev',
          id: 12158001,
        })
      ]);
    }, 0);


    // Test Execution
    const userInfo = await getUser('dev');

    // Test Verification
    expect(userInfo).to.have.property('login');
    expect(userInfo).to.have.property('id');
  });

  it('should return error for a non-existing user', async () => {
    // Test setup
    server = sinon.fakeServer.create();
    setTimeout(() => {
      server.respond([
        404,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          message: 'User not found',
        })
      ]);
    }, 0);


    // Test Execution
    const userInfo = await getUser('notavaliduser');

    // Test Verification
    expect(userInfo).to.not.have.property('login');
    expect(userInfo).to.not.have.property('id');
    expect(userInfo).to.have.property('message');
  });
});
